package helloworld;
public class Application {
    public static void main( String[] args )
    {
        System.out.println( "In JVM called main method" );
        System.out.println("Before calling overloaded main method");
        //System.out.println("Result of calling overloaded main method from JVM called" + 
          // "main method:");
       System.out.println(main(10,20));
    }
    public static int main(int a,int b){
        System.out.println( "In overloaded main method" );
        return a + b;
    }
}
/*Output :
In JVM called main method
Before calling overloaded main method
In overloaded main method
Result of Calling overloaded main method from JVM called main method:30

So we can see from above example that 
- When we ran Application.java ,JVM called main method with following signature only
 public static void main( String[] args );
- We can have overloaded version of main method also.Here we had overloaded main method with following signature
 public static int main(int a,int b);
- We can call overloaded main method just like any other http://method.In our example we called overloaded main method from JVM called main method as below
 System.out.println("Result of calling overloaded main method from JVM called" + "main method:"+main(10,20));

You can check here further,if we can override main method or not Can we overload or override main method in Java
1.6k Views · View Upvotes
Upvote4*/