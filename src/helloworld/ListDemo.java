package helloworld;
public class ListDemo {
	public static final String input = "(id,created,employee(id,firstname,employeeType(id), lastname),location)";
	public static final char OPEN_BRACKET = '(', CLOSE_BRACKET = ')', COMMA = ',', SPACE = ' ';

	public static void main(String[] args) {
		String prefix = "", curWord;
		for (int i = 1; i < input.length(); ++i) {
						if (input.charAt(i) == OPEN_BRACKET) {
				prefix += "-";
			} else if (input.charAt(i) == COMMA || input.charAt(i) == SPACE) {

			} else if (input.charAt(i) == CLOSE_BRACKET) {

				if (prefix.length() > 0)
					prefix = prefix.substring(1);
			} else {
				curWord = "";
				while (i < input.length()) {
					if (Character.isAlphabetic(input.charAt(i))) {
						curWord += input.charAt(i);
						++i;
					} else {
						
						System.out.println(prefix + " " + curWord + "\n");
						curWord = "";
						--i;
						break;
					}
				}
			}
		}
	}
}
